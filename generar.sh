#!/bin/bash

for file in "$@"
do
	for i in {3,5}
	do
		instance="$(basename "$file")"
		instance="${instance%.*}"
		resultsfile="resultados/$instance"_"$i"_results.txt
		./Debug/P2Metaheuristics $file $i > "$resultsfile"

		output="resultados/$instance"_"$i".png

cat << _end_ | gnuplot
	set terminal png
	set output "$output"
	set key right bottom
	set xlabel "Numero de ejecuciones"
	set ylabel "Fitness"
	plot '$resultsfile' using 1 with linespoints pi 10000 title 'Current First', '$resultsfile' using 2 with linespoints pi 10000 title 'Best First', '$resultsfile' using 3 with linespoints pi 10000 title 'Current Best', '$resultsfile' using 4 with linespoints pi 10000 title 'Best Best'
_end_
	
	echo -e "Generado: $output"

	done


done



#echo -e "Graphic generated: $2"